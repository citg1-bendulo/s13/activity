package com.zuitt.discussion.services;

import com.zuitt.discussion.models.Post;
import com.zuitt.discussion.repositories.PostRepository;
import org.apache.coyote.Response;
import org.springframework.http.ResponseEntity;

public interface PostService {


    void createPost(Post post);
    Iterable<Post> getPosts();
    ResponseEntity deletePost(Long id);

    ResponseEntity updatePost(Long id,Post post);
}
